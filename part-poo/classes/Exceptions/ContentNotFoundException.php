<?php

namespace Exceptions;

/**
 * Class ContentNotFoundException
 * @package Exceptions
 */
class ContentNotFoundException extends \Exception
{
    /**
     * ContentNotFoundException constructor.
     * 
     * @param string $id
     */
    public function __construct($id)
    {
        $message = "Content not found (id: $id)";

        parent::__construct($message);
    }
}