<?php

namespace Interfaces;

/**
 * Interface ItemInterface
 * @package Interfaces
 */
interface ItemInterface
{
    /**
     * @param int $id
     * @return mixed
     */
    public function getById($id);

    /**
     * @param $offset
     * @param $limit
     * @return mixed
     */
    public function getAll($offset = null, $limit = null);
}