<?php

namespace Manager;

require_once './classes/Exceptions/ContentNotFoundException.php';
require_once './classes/Manager/DbManager.php';
require_once './classes/Interfaces/ItemInterface.php';
require_once './classes/Content/Content.php';

use Content\Content;
use Interfaces\ItemInterface;
use Exceptions\ContentNotFoundException;

/**
 * Manage content from database
 * 
 * Class ContentManager
 * @package Manager
 */
class ContentManager extends DbManager implements ItemInterface
{
    /**
     * @param int $id
     * @return Content
     * @throws ContentNotFoundException
     */
    public function getById($id)
    {
        // Select content into database
        $stmt = $this->getDb()->prepare("SELECT * FROM articles WHERE id = :id;");
        $stmt->bindValue(":id", $id);
        $stmt->execute();

        // Throw an exception if the content hasn't been found
        if ($stmt->rowCount() === 0) {
            throw new ContentNotFoundException($id);
        }

        // Get new content
        $article = new Content();
        $article->hydrate(
            $stmt->fetch(\PDO::FETCH_ASSOC)
        );

        return $article;
    }

    /**
     * @param null $offset
     * @param null $limit
     * @return Content[]
     */
    public function getAll($offset = null, $limit = null)
    {
        // Select list of content in database
        $stmt = $this->getDb()->prepare("SELECT * FROM articles;");
        $stmt->execute();

        // Instantiates a collection of content
        $articles = array();
        while ($articleDatas = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $article = new Content();
            $article->hydrate($articleDatas);
            $articles[] = $article;
        }

        return $articles;
    }
}