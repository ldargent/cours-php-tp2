<?php

namespace Manager;

/**
 * Database connection manager
 *
 * Class DbManager
 */
abstract class DbManager
{
    /** @var \PDO */
    private static $db;

    /**
     * Initialize database connection and return it
     *
     * @return \PDO
     */
    public function getDb()
    {
        // Connecting to the database if it's not done yet
        if (is_null(self::$db)) {
            $pdo = new \PDO('mysql:host=127.0.0.1;dbname=tp_iut_blog;charset=utf8', 'root', 'kaliop');
            $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            self::setDb($pdo);
        }

        return self::$db;
    }

    /**
     * Set database connection
     *
     * @param \PDO $db
     */
    static private function setDb($db)
    {
        self::$db = $db;
    }
}