<?php
$connectUser = false;

// Check if user is connect
if (isset($_SESSION['id'])) {

    $stmt = $db->prepare('
            SELECT *
            FROM users AS u
            WHERE u.id = :id
        ');

    // Get user infos
    $stmt->bindParam(':id', $_SESSION['id']);
    if ($stmt->execute()) {
        $connectUser = $stmt->fetch();
    }
}
?>

<div id="navbar" class="navbar-collapse collapse">
    <?php if (!$connectUser) : ?>
        <form class="navbar-form navbar-right" role="form" method="post" action="login.php">
            <div class="form-group">
                <input type="text" placeholder="Login" name="login" class="form-control">
            </div>
            <div class="form-group">
                <input type="password" placeholder="Password" name="password" class="form-control">
            </div>
            <button type="submit" class="btn btn-success">Sign in</button>
        </form>
    <?php else : ?>
        <ul class="nav navbar-nav navbar-right">
            <li class="active">
                <a href="#"><?php echo $connectUser['name']; ?></a>
            </li>
            <li>
                <a href="logout.php">Log out</a>
            </li>
        </ul>
    <?php endif; ?>

</div><!--/.navbar-collapse -->