<?php

// Start session user
session_start();

include 'include/db.php';

// Connect to the database
$db = getDb();

$errorConnect = false;

if ((isset($_POST['login']) && !empty($_POST['login'])) &&
    (isset($_POST['password']) && !empty($_POST['password']))) {

    // Get post data and clean up
    $login = $_POST['login'];
    $password = sha1($_POST['password']);

    // Check into database for user with login + password
    $stmt = $db->prepare('
        SELECT *
        FROM users AS u
        WHERE u.login = :login
          AND u.password = :password
    ');

    $stmt->bindParam(':login', $login);
    $stmt->bindParam(':password', $password);

    if ($stmt->execute()) {

        // If there is one result, we found the user (with correct password)
        if ($stmt->rowCount() == 1) {

            // get db user info
            $user = $stmt->fetch();

            // set session with record id user
            $_SESSION['id'] = $user['id'];

            // update user's number of connection
            $updateNbConnect = $db->prepare("
                UPDATE users SET nb_connect = (nb_connect+1)
                WHERE id = :id
            ");
            $updateNbConnect->bindParam(':id', $user['id'], PDO::PARAM_INT);
            try{
                $updateNbConnect->execute();
            } catch (Exception $ex) {
                $errorConnect = $ex->getMessage();
            }

            // homepage redirect
            if (!$errorConnect) {
                header('location: index.php');
            }

        } else {
            // Set error message 'bad credentials'
            $errorConnect = "User login or password incorrect.";
        }
    }
} else {
    // Set error message 'login/password require'
    $errorConnect = "You must fill login and password fields.";
}
?>

<!doctype html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <style>
        body {
            padding-top: 50px;
            padding-bottom: 20px;
        }
    </style>
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="css/main.css">

    <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Home</a>
        </div>
        <?php include 'include/layout/login_form.php'; ?>
    </div>
</nav>

<!-- Main jumbotron for a primary marketing message or call to action -->
<div class="jumbotron">
    <div class="container">
        <h1>Php TP 2!</h1>
        <p>Project description</p>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-lg-8">
            <?php
            // Display error messages
            if ($errorConnect !== false) {
                echo '<div class="alert alert-danger">'.$errorConnect.'</div>';
            }
            ?>
        </div>
        <!-- Blog Sidebar Widgets Column -->
        <div class="col-md-4">
            <!-- Blog Search Well -->
            <div class="well">
                <h4>Blog Search</h4>
                <div class="input-group">
                    <input type="text" class="form-control">
                    <span class="input-group-btn">
                    <button class="btn btn-default" type="button">
                    <span class="glyphicon glyphicon-search"></span>
                    </button>
                    </span>
                </div>
                <!-- /.input-group -->
            </div>
            <!-- Blog Categories Well -->
            <div class="well">
                <h4>Blog Categories</h4>
                <div class="row">
                    <div class="col-lg-6">
                        <ul class="list-unstyled">
                            <li><a href="#">Category Name</a>
                            </li>
                            <li><a href="#">Category Name</a>
                            </li>
                            <li><a href="#">Category Name</a>
                            </li>
                            <li><a href="#">Category Name</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-lg-6">
                        <ul class="list-unstyled">
                            <li><a href="#">Category Name</a>
                            </li>
                            <li><a href="#">Category Name</a>
                            </li>
                            <li><a href="#">Category Name</a>
                            </li>
                            <li><a href="#">Category Name</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- Side Widget Well -->
            <div class="well">
                <h4>Side Widget Well</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore, perspiciatis adipisci accusamus
                    laudantium odit aliquam repellat tempore quos aspernatur vero.</p>
            </div>
        </div>
    </div>

    <hr>

    <!-- Footer -->
    <footer>
        <div class="row">
            <div class="col-lg-12">
                <p>TP Blog - IUT Montpellier</p>
            </div>
        </div>
    </footer>
</div> <!-- /container -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

<script src="js/vendor/bootstrap.min.js"></script>

<script src="js/main.js"></script>
</body>
</html>
