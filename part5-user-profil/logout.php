<?php
session_start();

// Destroy session
$_SESSION = array();
session_destroy();

header('location: index.php');