<?php

// Start session user
session_start();

include 'include/db.php';

// Connect to the database
$db = getDb();

$connectUser = false;
$errorPage = false;

// Check if user is connect
if (isset($_SESSION['id'])) {

    $infosUser = false;

    // Get user infos
    $stmt = $db->prepare('
            SELECT *
            FROM users AS u
            WHERE u.id = :id
        ');

    // Get user infos
    $stmt->bindParam(':id', $_SESSION['id']);
    if ($stmt->execute()) {
        $infosUser = $stmt->fetch();
    }
} else {
    $errorPage = "You are not allowed to view this page.";
}

// Display context error
if ($errorPage !== false) {
    echo '<div class="alert alert-danger">'.$errorPage.'</div>';
    exit;
}

// Check post datas
$warningMessageDisplay = "";
$updateMessageDisplay = "";
$warningMessages = array();

// Check email
if (isset($_POST['email'])) {
    if (!empty($_POST['email'])) {
        $infosUser['email'] = htmlspecialchars($_POST['email']);
    } else {
        $infosUser['email'] = '';
        $warningMessages[] = "Email cannot be empty.";
    }
}

// Check passwords
if ((isset($_POST['password1']) && !empty($_POST['password1'])) &&
    (isset($_POST['password2']) && !empty($_POST['password2']))) {

    $password1 = $_POST['password1'];
    $password2 = $_POST['password2'];

    if ($password1 != $password2) {
        $warningMessages[] = "Passwords are different.";
    } else {
        $infosUser['password'] = sha1($password1);
    }
}

// Display warning messages
foreach ($warningMessages as $warningMessage) {
    $warningMessageDisplay .= '<div class="alert alert-warning">'.$warningMessage.'</div>';
}

// Update user
$updateError = false;
$updateSuccess = false;
if (count($_POST) > 0 && count($warningMessages) === 0) {

    // update user's number of connection
    $updateUserInfos = $db->prepare("
                UPDATE users SET email = :email, password = :password
                WHERE id = :id
            ");
    $updateUserInfos->bindParam(':email', $infosUser['email']);
    $updateUserInfos->bindParam(':password', $infosUser['password']);
    $updateUserInfos->bindParam(':id', $infosUser['id']);
    try{
        $updateUserInfos->execute();
        $updateSuccess = "User update successful.";
    } catch (Exception $ex) {
        $updateError = $ex->getMessage();
    }
}

// Error message
if ($updateError !== false) {
    $updateMessageDisplay = '<div class="alert alert-danger">'.$updateError.'</div>';
}

// Update message
if ($updateSuccess !== false) {
    $updateMessageDisplay = '<div class="alert alert-success">'.$updateSuccess.'</div>';
}

?>
<!doctype html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <style>
        body {
            padding-top: 50px;
            padding-bottom: 20px;
        }
    </style>
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="css/main.css">

    <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php">Home</a>
        </div>
        <?php include 'include/layout/login_form.php'; ?>
    </div>
</nav>

<!-- Main jumbotron for a primary marketing message or call to action -->
<div class="jumbotron">
    <div class="container">
        <h1>Php TP 2!</h1>
        <p>Project description</p>
    </div>
</div>

<!-- Page Content -->
<div class="container">
    <div class="row">
        <!-- Blog Post Content Column -->
        <div class="col-lg-8">
            <ol class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">Library</a></li>
                <li class="active">Data</li>
            </ol>
            <?php
                // Display error messages
                if ($warningMessageDisplay !== "") {
                    echo $warningMessageDisplay;
                }
                if ($updateMessageDisplay !== "") {
                    echo $updateMessageDisplay;
                }
            ?>
            <div class="panel panel-info">
                <div class="panel-heading">Account profile</div>
                <div class="panel-body">
                    <form role="form" method="post">
                        <div class="form-group">
                            <label class="control-label" for="inputLogin">Login</label>
                            <input type="text" class="form-control" id="inputLogin" name="login" disabled="disabled" value="<?php echo $infosUser['login']; ?>">
                            <p class="help-block">Your user login. You cannot edit it.</p>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="inputEmail">Email</label>
                            <input type="email" class="form-control" id="inputEmail" name="email" placeholder="Enter email" value="<?php echo $infosUser['email']; ?>">
                            <p class="help-block">Put your email here (johndoe@montpellier.fr)</p>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="inputPassword1">Password</label>
                            <input type="password" class="form-control" id="inputPassword1" name="password1" placeholder="Password">
                            <input type="password" class="form-control" id="inputPassword2" name="password2" placeholder="Password confirmation">
                            <p class="help-block">Put your password twice here</p>
                        </div>

                        <!--
                        <div class="form-group">
                            <label class="control-label" for="exampleInputFile">File</label>
                            <input type="file" id="exampleInputFile">
                            <p class="help-block">Example block-level help text here.</p>
                        </div>
                        -->

                        <div class="form-group">
                            <label class="control-label" for="inputNbConnect">Connection count</label>
                            <input type="text" class="form-control" id="inputNbConnect" disabled="disabled" value="<?php echo $infosUser['nb_connect']; ?>">
                            <p class="help-block">Number of time that you sign in on the blog.</p>
                        </div>

                        <button type="submit" class="btn btn-default">Update details</button>
                    </form>

                </div>
            </div>
        </div>
        <!-- Blog Sidebar Widgets Column -->
        <div class="col-md-4">
            <!-- Blog Search Well -->
            <div class="well">
                <h4>Blog Search</h4>
                <div class="input-group">
                    <input type="text" class="form-control">
                    <span class="input-group-btn">
                    <button class="btn btn-default" type="button">
                    <span class="glyphicon glyphicon-search"></span>
                    </button>
                    </span>
                </div>
                <!-- /.input-group -->
            </div>
            <!-- Blog Categories Well -->
            <div class="well">
                <h4>Blog Categories</h4>
                <div class="row">
                    <div class="col-lg-6">
                        <ul class="list-unstyled">
                            <li><a href="#">Category Name</a>
                            </li>
                            <li><a href="#">Category Name</a>
                            </li>
                            <li><a href="#">Category Name</a>
                            </li>
                            <li><a href="#">Category Name</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-lg-6">
                        <ul class="list-unstyled">
                            <li><a href="#">Category Name</a>
                            </li>
                            <li><a href="#">Category Name</a>
                            </li>
                            <li><a href="#">Category Name</a>
                            </li>
                            <li><a href="#">Category Name</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- Side Widget Well -->
            <div class="well">
                <h4>Side Widget Well</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore, perspiciatis adipisci accusamus
                    laudantium odit aliquam repellat tempore quos aspernatur vero.</p>
            </div>
        </div>
    </div>
    <!-- /.row -->
    <hr>
    <!-- Footer -->
    <footer>
        <div class="row">
            <div class="col-lg-12">
                <p>TP Blog - IUT Montpellier</p>
            </div>
        </div>
    </footer>
</div>
<!-- /.container -->

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

<script src="js/vendor/bootstrap.min.js"></script>

<script src="js/main.js"></script>
</body>
</html>
